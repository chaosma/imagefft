"""
Usage:
   fft.py -s FILENAME [-n FREQUENCY] [-f FACTOR] [--lin] [-v]
   fft.py (-h | --help)

Examples:
   python fft.py -s inputimage.tif
   python fft.py -s inputimage.tif --lin -v
   python fft.py -s sinwave.tif -n 50 -f 0.00001

Options:
   -s FILENAME         The name of input image. If -n is specified,the image will be created.
   -n FREQUENCY        frequency of sine wave, this option will create input image with name given by -s option
   -f FACTOR           multiply FACTOR to output fft image before scaling, only effective when using logarithm normalization
   -h --help          Show this screen
   --lin               Use linear normalization. If not specified, the default logarithm normalization will be used.
   -v                  verbose, check args information for debug purpose
"""

import numpy as np
from PIL import Image
import sys
from docopt import docopt


#read an image and return its data with double precision
def readImage(filename):
    im = Image.open(filename)
    width = im.width
    height = im.height
#    data = np.zeros((width,height))
    data = np.array(im)
    return data.astype(float)  #float is double precision

# swap first<-->third, second<-->fourth quadrant
def fftshift(data):
# assuming data is numpy 2d array
    nr = data.shape[0]
    nc = data.shape[1]
    assert (nr%2 == 0) & (nc%2 == 0)

    tmp = np.copy(data[0:nr/2,0:nc/2])
    data[0:nr/2,0:nc/2] = np.copy(data[nr/2:,nc/2:])
    data[nr/2:,nc/2:] = np.copy(tmp)

    tmp = np.copy(data[0:nr/2,nc/2:])
    data[0:nr/2,nc/2:] = np.copy(data[nr/2:,0:nc/2])
    data[nr/2:,0:nc/2] = np.copy(tmp)

#when lin = True, factor take no effect. Actually, it is the limiting case of lin = False and factor->0
def normalizefft(data,lin=False, factor=1.):
    fftshift(data)
    data = factor*data
    if lin:
        ndata = np.absolute(data)
    else:
        ndata = np.log(np.absolute(data)+1)

    ndata = ndata*255./ndata.max()

    return ndata


def writeImage(data,filename):
    im = Image.fromarray(data)
    im.save(filename)


def generateImage(freq,filename):
    indexrow = np.linspace(0., 2.*np.pi*freq, num=128)
    sinrow = (np.sin(indexrow) + 1.) * 128.
    img = []
    for row in range(128):
        img.append(sinrow)

    img = np.array(img,dtype='uint8')
    fimg  = Image.fromarray(img)
    fimg.save(filename)



def fft2d(data):
    nr = data.shape[0]
    nc = data.shape[1]
    ndata = np.zeros((nr,nc),dtype=complex)

    for i in range(nr):
        ndata[i,:] = np.fft.fft(data[i,:])

    for i in range(nc):
        ndata[:,i] = np.fft.fft(ndata[:,i])

    return ndata


def main():

    args = docopt(__doc__)

    if args['-v']:
        print args

    inputfile = args['-s']
    outputfile = inputfile + "_fft.tif"

    if args['-n']:
        try:
            freq = float(args['-n'])
        except ValueError:
            print "the input of --freq should be a float number..."
        generateImage(freq,inputfile)

    data = readImage(inputfile)
    fftdata = np.fft.fft2(data)
   # fftdata1 = fft2d(data)
   # print np.allclose(fftdata,fftdata1)

    if args['-f']:
        try:
            factor = float(args['-f'])
        except ValueError:
            print "the factor should be a float number"
    else:
        factor = 1.

    ndata = normalizefft(fftdata,args['--lin'],factor)

    ndata = ndata.astype('uint8')
    writeImage(ndata,outputfile)

if __name__ == "__main__":
    main()

