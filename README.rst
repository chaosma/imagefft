image_fft
=========

Fast fourier transform of image data into fft data and write the output image with the name input_image_name+"_fft.tif"

Requirements
------------
numpy, PIL, docopt 

``pip install pillow``

``pip install numpy``

``pip install docopt``


Usage
------

``fft.py -s FILENAME [-n FREQUENCY] [-f FACTOR] [--lin] [-v]``

``fft.py (-h | --help)``

Examples
--------

``python fft.py -s inputimage.tif``

``python fft.py -s inputimage.tif --lin -v``

``python fft.py -s sinwave.tif -n 50 -f 0.00001``

Options
--------
   -s FILENAME         The name of input image. If -n is specified,the image will be created.
   
   -n FREQUENCY        frequency of sine wave, this option will create input image with name given by -s option

   -f FACTOR           multiply FACTOR to output fft image before scaling

   -h, --help          Show this screen

   --lin               Use linear normalization. If not specified, the default logarithm normalization will be used.

   -v                  verbose, check args information for debug purpose










